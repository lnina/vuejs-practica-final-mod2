# modulo-dos

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


### Administrador

usuario: admin
password: admin
Rol: Administrador

### Técnico

usuario: tecnico
password: tecnico
Rol: Técnico