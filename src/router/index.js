import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const getTokenYTipo = () => {
  const tokenAdmin = window.localStorage.getItem('token-adm')
  const tokenTec = window.localStorage.getItem('token-tec')

  if (tokenAdmin) {
    window.localStorage.removeItem('token-tec')
    return {
      tipo: 'adm',
      token: tokenAdmin
    }
  }
  if (tokenTec) {
    window.localStorage.removeItem('token-adm')
    return {
      tipo: 'tec',
      token: tokenTec
    }
  }
  return {
    tipo: '',
    token: null
  }
}

const guardAppAdmin = (to, from, next) => {
  //const _existeToken = window.localStorage.getItem('token')
  const _existeToken = window.localStorage.getItem('token-adm')
  if (_existeToken) {
    next()
  } else {
    next('/')
  }
}
const guardAppTec = (to, from, next) => {
  //const _existeToken = window.localStorage.getItem('token')
  const _existeToken = window.localStorage.getItem('token-tec')
  if (_existeToken) {
    next()
  } else {
    next('/app/tecnico')
  }
}

const guardLogin = (to, from, next) => {
  //const _existeToken = window.localStorage.getItem('token')
  const { tipo , token } = getTokenYTipo ()
  const _existeToken = token
  if (_existeToken) {
    if (tipo == 'adm') {
      return next('/app/admin')
    }
    else if (tipo == 'tec') {
      return next('/app/tecnico')
    }
    else {
      return next('/')
    }
  }
  return next()
}

const routes = [
  {
    path: '/',
    component: () => import('../layouts/OutLoginLayout.vue'),
    beforeEnter: guardLogin,
    children: [
      {
        path: '',
        name: 'Login',
        component: () => import('../views/Login.vue')
      }
    ]
  },
  /*{
    path: '/app',
    component: () => import('../layouts/MainLayout.vue'),
    beforeEnter: guardApp,
    children: [
      {
        path: '',
        name: 'Inicio',
        component: () => import('../views/Inicio.vue')
      },
      {
        path: 'usuarios',
        name: 'Usuario',
        component: () => import('../views/Usuario.vue')
      },
      {
        path: 'roles',
        name: 'Rol',
        component: () => import('../views/Rol.vue')
      },
      {
        path: 'admin',
        name: 'Administrador',
        component: () => import('../views/Administrador.vue')
      },
      {
        path: 'tecnico',
        name: 'Tecnico',
        component: () => import('../views/Tecnico.vue')
      },
    ]
  },*/
  {
    path: '/app',
    component: () => import('../layouts/MainLayoutAdmin.vue'),
    beforeEnter: guardAppAdmin,
    children: [
      {
        path: '',
        name: 'Inicio',
        component: () => import('../views/Inicio.vue')
      },
      {
        path: 'usuarios',
        name: 'Usuario',
        component: () => import('../views/Usuario.vue')
      },
      {
        path: 'roles',
        name: 'Rol',
        component: () => import('../views/Rol.vue')
      },
      {
        path: 'admin',
        name: 'Administrador',
        component: () => import('../views/Administrador.vue')
      }
    ]
  },
  {
    path: '/app',
    component: () => import('../layouts/MainLayoutTecnico.vue'),
    beforeEnter: guardAppTec,
    children: [
      {
        path: 'tecnico',
        name: 'Tecnico',
        component: () => import('../views/Tecnico.vue')
      },
      {
        path: 'datos',
        name: 'Datos',
        component: () => import('../views/Datos.vue')
      }
    ]
  }
]

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})

export default router
