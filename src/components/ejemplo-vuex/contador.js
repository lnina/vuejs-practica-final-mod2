import { mapState } from 'vuex'

export default {
  computed: {
    ...mapState(['contador', 'titulo'])
  },
  methods: {
    incrementar (incremento = 1) {
      this.$store.commit('incrementar', incremento)
    },
    decrementar (decremento = 1) {
      this.$store.commit('decrementar', decremento)
    }
  }
}
