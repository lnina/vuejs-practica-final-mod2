import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    titulo: 'APLICACION VUE',
    lista: [],
    visible: false,
    contador: 0,
    listaNotas: [
      {
        titulo: 'Nota 1',
        descripcion: 'Descripcion 1'
      }
    ]
  },
  mutations: {
    setVisible (state) {
      state.visible = !state.visible
    },
    setTitulo (state, titulo) {
      state.titulo = titulo
    },
    incrementar (state, incremento = 1) {
      state.contador += incremento
    },
    decrementar (state, decremento = 1) {
      state.contador -= decremento
    },
    adicionarNota (state, nota) {
      state.listaNotas.push(nota)
    },
    eliminarNota (state, index) {
      state.listaNotas.splice(index, 1)
    }
  },
  actions: {
  },
  modules: {
  }
})
